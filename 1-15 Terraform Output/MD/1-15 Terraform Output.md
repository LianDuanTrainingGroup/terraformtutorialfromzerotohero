
<br>
<br>
📌

# **<image src='./images/terraform.svg' width="5%" hight="5%"/>Advanced Terraform Output Techniques**  

##  **🎯Topics**
- **<h2>🤔What is Terraform Output?</h2>**


- **<h2>🕵🏻Terraform Output Using Scenarios</h2>**


- **<h2>✍Hands-on Demos </h2>**



<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌



## **🤔What is Terraform Output?** 

- **<h2> 🤳🏾Display Useful Information </h2>**       

  - **<h3>Syntax</h3>**    
```
output "<OUTPUT_NAME>" {
  value = <OUTPUT_VALUE>
  sensitive = true/false
}

output "<OUTPUT_NAME>" {
  value = {
     <OUTPUT_KEY> = <OUTPUT_VALUE>
     <OUTPUT_KEY> = <OUTPUT_VALUE>
     ...
  } 
}
```

- **<h2>📲Retrieve and Display Attributes of Resources</h2>**  

  - **<h3>Terraform Command</h3>**  

```
terraform output [OPTIONAL_FLAGS] [NAME]
terraform output --help
```

<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌




## **🕵🏻Terraform Output Using Scenarios**  


- **<h2>👀View Resource </h2>** 


  - Debugging
  


- **<h2>📰Output Value </h2>** 
 
  - Testing(Scripting)
  - Share Resource  
- **<h2>🎫Pass Value between Modules </h2>** 



<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌



## **✍Hands-on Demos**  


**<h2>🔡Prerequisites</h2>**   



-  **<h2>🎥[Install Terraform on Windows 11](https://youtu.be/5D76gEuAujk)</h2>**
   
-  **<h2>📽️[Install Terraform on Ubuntu 23](https://youtu.be/f5BXa6Xk1Ws)</h2>**
  
-  **<h2>📽 [Configure AWS CLI](https://youtu.be/P10P2NrpzpU)</h2>**
-  **<h2>🏷️Terraform AWS Providers Version: 5.8.0</h2>**

<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌



##  **👉Hands-on Demo: Output Single Value**
**<h3>📟Code: AWS VPC [main.tf](https://gitlab.com/LianDuanTrainingGroup/terraformtutorialfromzerotohero/-/blob/main/1-15%20Terraform%20Output/Code/AWS%20VPC/main.tf?ref_type=heads)</h3>**
```
# Output the vpc id
output "vpc_id" {
  value = aws_vpc.demo-vpc-01.id
  sensitive = false
}
```
**<h3>🤖terraform output vpc_id</h3>**  


<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌


##  **👉Hands-on Demo:Output Multiple Values** 

**<h3>📟Code: AWS VPC [main.tf](https://gitlab.com/LianDuanTrainingGroup/terraformtutorialfromzerotohero/-/blob/main/1-15%20Terraform%20Output/Code/AWS%20VPC/main.tf?ref_type=heads)</h3>**

```
# Output the vpc detail
output "vpc_detail" {
  value = {
    vpc_id   = aws_vpc.demo-vpc-01.id
    cidr_block = aws_vpc.demo-vpc-01.cidr_block
  }
}

# Output the subnet IDs
output "subnet_arns" {
  value = aws_subnet.public[*].arn
}

```

<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌




##  **👉Hands-on Demo: Pass Values From VPC Module to ECS Module**
**<h3>🎦[Terraform Create AWS ECS](https://gitlab.com/LianDuanTrainingGroup/terraformtutorialfromzerotohero/-/tree/main/1-15%20Terraform%20Output/Code/AWS%20ECS?ref_type=heads)</h3>**  


**<h3>📟Code: AWS ECS [main.tf](https://gitlab.com/LianDuanTrainingGroup/terraformtutorialfromzerotohero/-/tree/main/1-15%20Terraform%20Output/Code/AWS%20ECS?ref_type=heads)</h3>**   


  <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content"  width="100%"><image src='./images/files.jpg'   width="300px" style="vertical-align:top"> </div>

<br>



**network/outputs.tf**
```
network/outputs.tf
output "vpc_id" {
  value = aws_vpc.demo-vpc-01.id
}
```

**ecs/main.tf**

```
variable "vpc_id" {
  description = "The ID of the VPC where the ECS service will be deployed."
}
```
**main.tf**

```
# network
module "network" {
  source = "./modules/network"
}
module "ecs" {
  source = "./modules/ecs"
# vpc_id            = module.provider_module.output_name
  vpc_id            = module.network.vpc_id
  subnet_ids        = module.network.subnet_ids
  target_group_arn  = module.network.load_balancer_target_group_arn
  lb_listener_dependency = module.network.lb_listener_dependency
  load_balancer_security_group_id = module.network.load_balancer_security_group_id
}
```


<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌



# 

AWS_ECS/
   |- main.tf
   |- outputs.tf
   |- modules/
       |- network/
          |- main.tf
          |- outputs.tf
       |- ecs/
          |- main.tf
          |- outputs.tf
