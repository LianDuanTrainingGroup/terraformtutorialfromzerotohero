# ecs/main.tf

variable "vpc_id" {
  description = "The ID of the VPC where the ECS service will be deployed."
}

variable "subnet_ids" {
  description = "The list of subnet IDs where the ECS service will be deployed."
  type        = list(string)
}

variable "target_group_arn" {
  description = "The ARN of the target group for the ECS service."
}

variable "lb_listener_dependency" {
  description = "Dependency on the LB listener for the ECS service."
}

variable "load_balancer_security_group_id" {
  description = "load_balancer_security_group_id"
}

resource "aws_ecs_task_definition" "demo-nginx-task-definition" {
  family                   = "demo-task-nginx"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 1024
  memory                   = 2048
  container_definitions = jsonencode([
    {
      name  = "demo-test-container"
      image = "public.ecr.aws/q8h0q2b2/terraformdemo:v1"
      cpu   = 1024
      memory = 2048
      portMappings = [
        {
          containerPort = 80
          protocol      = "tcp"
          hostPort      = 80
        }
      ]
    }
  ])
}

resource "aws_security_group" "demo-task-security-group-01" {
  name   = "demo-task-security-group-01"
  vpc_id = var.vpc_id

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    security_groups = [var.load_balancer_security_group_id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_ecs_service" "demo-hello-world-service" {
  name            = "demo-hello-world-service"
  cluster         = aws_ecs_cluster.demo-fargate-cluster.id
  task_definition = aws_ecs_task_definition.demo-nginx-task-definition.arn
  desired_count   = 2
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = [aws_security_group.demo-task-security-group-01.id]
    subnets         = var.subnet_ids
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = var.target_group_arn
    container_name   = "demo-test-container"
    container_port   = 80
  }

  depends_on = [var.lb_listener_dependency]
}

resource "aws_ecs_cluster" "demo-fargate-cluster" {
  name = "demo-fargate-cluster"
}
