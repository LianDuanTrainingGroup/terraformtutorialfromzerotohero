# ecs/outputs.tf
output "service_name" {
  value = aws_ecs_service.demo-hello-world-service.name
}

output "task_definition_family" {
  value = aws_ecs_task_definition.demo-nginx-task-definition.family
}
