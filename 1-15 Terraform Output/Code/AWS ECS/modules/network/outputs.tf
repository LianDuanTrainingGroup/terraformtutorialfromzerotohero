# network/outputs.tf
output "vpc_id" {
  value = aws_vpc.demo-vpc-01.id
}

output "subnet_ids" {
  value = [aws_subnet.demo-public-01.id, aws_subnet.demo-public-02.id]
}

output "load_balancer_dns" {
  value = aws_lb.demo-lb-01.dns_name
}

output "load_balancer_target_group_arn" {
  value = aws_lb_target_group.demo-target-group-01.arn
}

output "lb_listener_dependency" {
  value = aws_lb_listener.demo-lb-lis-01
}

output "load_balancer_security_group_id" {
  value = aws_security_group.demo-lb-security-group-01.id
}