# network/main.tf


resource "aws_vpc" "demo-vpc-01" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "demo-vpc-01"
  }
}

resource "aws_subnet" "demo-public-01" {
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-east-2a"
  vpc_id                  = aws_vpc.demo-vpc-01.id
  map_public_ip_on_launch = true
  tags = {
    Name = "demo-public-01"
  }
}

resource "aws_subnet" "demo-public-02" {
  cidr_block              = "10.0.2.0/24"
  availability_zone       = "us-east-2b"
  vpc_id                  = aws_vpc.demo-vpc-01.id
  map_public_ip_on_launch = true
  tags = {
    Name = "demo-public-02"
  }
}

resource "aws_internet_gateway" "demo-internet-gateway-01" {
  vpc_id = aws_vpc.demo-vpc-01.id
  tags = {
    Name = "demo-internet-gateway-01"
  }
}

resource "aws_route" "internet_access" {
  route_table_id         = aws_vpc.demo-vpc-01.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.demo-internet-gateway-01.id
}

resource "aws_eip" "demo-eip-gateway-01" {
  #vpc        = true
  domain   = "vpc"
  depends_on = [aws_internet_gateway.demo-internet-gateway-01]
  tags = {
    Name = "demo-eip-gateway-01"
  }
}

resource "aws_security_group" "demo-lb-security-group-01" {
  name   = "demo-lb-security-group-01"
  vpc_id = aws_vpc.demo-vpc-01.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb" "demo-lb-01" {
  name            = "demo-lb-01"
  subnets         = [aws_subnet.demo-public-01.id, aws_subnet.demo-public-02.id]
  security_groups = [aws_security_group.demo-lb-security-group-01.id]
}

resource "aws_lb_target_group" "demo-target-group-01" {
  name        = "demo-target-group-01"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.demo-vpc-01.id
  target_type = "ip"
}

resource "aws_lb_listener" "demo-lb-lis-01" {
  load_balancer_arn = aws_lb.demo-lb-01.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.demo-target-group-01.id
    type             = "forward"
  }
}

output "load_balancer_ip" {
  value = aws_lb.demo-lb-01.dns_name
}
