# outputs.tf
output "network_vpc_id" {
  value = module.network.vpc_id
}

output "network_subnet_ids" {
  value = module.network.subnet_ids
}

output "network_load_balancer_dns" {
  value = module.network.load_balancer_dns
}

output "ecs_service_name" {
  value = module.ecs.service_name
}

output "ecs_task_definition_family" {
  value = module.ecs.task_definition_family
}
