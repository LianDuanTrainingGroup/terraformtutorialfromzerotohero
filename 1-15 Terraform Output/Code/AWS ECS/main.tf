# //////////////////////////////
#          Providers
# //////////////////////////////
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.8.0"
    }
  }
}
# Configure the AWS Provider
provider "aws" {
  region = "us-east-2"

  default_tags {
    tags = {
      Name = "create-by-terraform-need-tag"
    }
  }
}

# network
module "network" {
  source = "./modules/network"
}

# ecs
module "ecs" {
  source = "./modules/ecs"
  vpc_id            = module.network.vpc_id
  subnet_ids        = module.network.subnet_ids
  target_group_arn  = module.network.load_balancer_target_group_arn
  lb_listener_dependency = module.network.lb_listener_dependency
  load_balancer_security_group_id = module.network.load_balancer_security_group_id
}
