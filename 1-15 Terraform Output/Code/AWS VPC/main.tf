# //////////////////////////////
#          Providers
# //////////////////////////////
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.8.0"
    }
  }
  /*
 backend "s3" {
    bucket = "demo-bucket-lian"
    region = "us-east-2"
    dynamodb_table = "demo-dynamodb-table-01"
  }
   */
}
# Configure the AWS Provider
provider "aws" {
  region = "us-east-2"

  default_tags {
    tags = {
      Name = "create-by-terraform-need-tag"
    }
  }
}


# //////////////////////////////
#          Resources
# //////////////////////////////

# VPC-01
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc

resource "aws_vpc" "demo-vpc-01" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "demo-vpc-01"
  }
}


# VPC-Public-Subnet
resource "aws_subnet" "public" {
  count                   = 2
  # https://developer.hashicorp.com/terraform/language/functions/cidrsubnet
  cidr_block              = cidrsubnet(aws_vpc.demo-vpc-01.cidr_block, 8, 2 + count.index)
  availability_zone       = "us-east-2a"
  vpc_id                  = aws_vpc.demo-vpc-01.id
  map_public_ip_on_launch = true
   tags = {
    Name = "demo-public-sub-${count.index}"
  }
}

# Output the vpc id
output "vpc_id" {
  value = aws_vpc.demo-vpc-01.id
  sensitive = true
 
}

# Output the vpc detail
output "vpc_detail" {
  value = {
    vpc_id   = aws_vpc.demo-vpc-01.id
    cidr_block = aws_vpc.demo-vpc-01.cidr_block
  }
}

# Output the subnet IDs
output "subnet_arns" {
  value = aws_subnet.public[*].arn
}

