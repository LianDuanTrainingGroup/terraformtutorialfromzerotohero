<br>      
📌

# **<image src='./images/terraform.svg' width="5%" hight="5%"/>Terraform 5 Init Command**   
## **⤵️Terraform 5 Init Command Steps**    
- **<h2>⚙️Init Backend Configuration(optional) </h2>**  
   - <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/wstep0.jpg'  width="600px" style="vertical-align:top"></div>

<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌



## **🗂️Create a .terraform Folder**  
   - <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/wstep1.jpg'  width="400px" style="vertical-align:top"></div>

<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌



## **📥Download Providers</h2>** 
  - <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/wstep2.jpg'  width="400px" style="vertical-align:top"></div> 
  
    - 🖼️**Windows**
         - <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/wstep3-1.jpg'  width="600px" style="vertical-align:top"></div> 
    - 🐧**Linux**
        - <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/lstep3-1.jpg'  width="600px" style="vertical-align:top"></div> 

<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌




## **📥Download External Models(optional)** 

```
      module "example_module" {
         source = "git::https://gitlab.com/your-group/your-repo.git"
          # Module input variables
         variable1 = "value1"
         variable2 = "value2"
      }
```  
- **✨Model Files** 
  - main.tf
  - variables.tf
  - outputs.tf


<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌




## **🔐Create a .terraform.lock.hcl File**

  - **📋Lock file**  
      - <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/loclfile.jpg'  width="600px" style="vertical-align:top"></div>
  - **📨Lock file message**  
      - <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/lock.jpg'  width="600px" style="vertical-align:top"></div>   
  
<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌




## **📊Create a terraform.tfstate File(optional)**
  - <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/wstep5.jpg'  width="600px" style="vertical-align:top"></div>

<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌



## **📒Finally Message**
  - <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/wstep10.jpg'  width="600px" style="vertical-align:top"></div>


<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌

