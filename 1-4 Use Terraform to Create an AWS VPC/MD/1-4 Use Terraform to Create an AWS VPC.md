# **<image src='./images/terraform.svg' width="5%" hight="5%"/>Use Terraform to Create AWS VPC**


#  **📆Topics** 

  -  **<h2>🔶Prerequisites</h2>**


  -  **<h2>📒Create Terraform File</h2>**


  -  **<h2>🚇Initialize Terraform</h2>**


  -  **<h2>🎯Create AWS VPC </h2>**



<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌

# **✅Prerequisites**
 - **<h2>👨🏽‍🔧Terraform</h2>**  


   - 🎞️[How to Install Terraform on Windows 11]()
   - 🎞️[Install Terraform on Ubuntu 23]()


 - **<h2>🪟AWS CLI (Administrator Access)</h2>**


   - 🎞️[Configure AWS CLI]() 
  
<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌


# **📒Create Terraform File**


## **<h2>🏜️main.tf</h2>**  







```
# PROVIDER
  provider "aws" {
    region = "us-east-2"
  }
# RESOURCE  
  resource "aws_vpc" "my_vpc_01" {
    cidr_block = "10.0.0.0/16"
        tags = {
            Name = "my_vpc_01"
        }
  }
```
📚[Providers](https://registry.terraform.io/browse/providers)

📚[Resource: aws_vpc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc)

<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌



# **🚇Initialize Terraform**
- 🕹️`terraform init`
  - <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/init.jpg'  width="600px" style="vertical-align:top"></div>

<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌



# **🎯Create AWS VPC**
- 🕹️`terraform apply`
  
  - <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/apply.jpg'  width="600px" style="vertical-align:top"></div>
  
</br>

## **🔎Verify VPC Creation**
  -🕹️`aws ec2 describe-vpcs --filters Name=tag:Name,Values=my_vpc_01`

  - <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/check.jpg'  width="600px" style="vertical-align:top"></div>


<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌


