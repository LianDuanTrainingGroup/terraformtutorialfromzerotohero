# //////////////////////////////
#          PROVIDERS
# //////////////////////////////
provider "aws" {
  region = "us-east-2"
}
# //////////////////////////////
#          RESOURCES
# //////////////////////////////
resource "aws_vpc" "my_vpc_01" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "my_vpc_01"
  }
}

# aws ec2 describe-vpcs --filters Name=tag:Name,Values=my_vpc_01