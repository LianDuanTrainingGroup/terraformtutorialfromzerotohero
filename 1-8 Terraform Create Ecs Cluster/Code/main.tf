# //////////////////////////////
#          Providers
# //////////////////////////////
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.0.1"
    }
  }
}
# Configure the AWS Provider
provider "aws" {
  region = "us-east-2"

  default_tags {
    tags = {
      Name = "create-by-terraform-need-tag"
    }
  }
}


# //////////////////////////////
#          Resources
# //////////////////////////////

# VPC-01
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc

resource "aws_vpc" "demo-vpc-01" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "demo-vpc-01"
  }
}
# VPC-02
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet
resource "aws_subnet" "demo-public-sub-01" {
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-east-2a"
  vpc_id                  = aws_vpc.demo-vpc-01.id
  map_public_ip_on_launch = true
  tags = {
    Name = "demo-public-sub-01"
  }
}



# VPC-03
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet
resource "aws_subnet" "demo-public-sub-02" {
  cidr_block              = "10.0.2.0/24"
  availability_zone       = "us-east-2b"
  vpc_id                  = aws_vpc.demo-vpc-01.id
  map_public_ip_on_launch = true
  tags = {
    Name = "demo-public-sub-02"
  }
}

# VPC-05
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway
resource "aws_internet_gateway" "demo-internet-gateway-01" {
  vpc_id = aws_vpc.demo-vpc-01.id
  tags = {
    Name = "demo-internet-gateway-01"
  }
}


# VPC-04
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route

resource "aws_route" "internet_access" {
  route_table_id         = aws_vpc.demo-vpc-01.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.demo-internet-gateway-01.id

}

# VPC-05
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip
resource "aws_eip" "demo-eip-gateway-01" {
  vpc        = true
  depends_on = [aws_internet_gateway.demo-internet-gateway-01]
  tags = {
    Name = "demo-eip-gateway-01"
  }
}



# ECS-04
# Create an ECS task definition
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition
resource "aws_ecs_task_definition" "demo-nginx-task-definition" {
  family                   = "demo-task-nginx"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  container_definitions = jsonencode([
    {
      name  = "demo-test-container"
      image = "public.ecr.aws/q8h0q2b2/terraformdemo:v1"
      cpu : 256
      memory = 256
      portMappings = [
        {
          containerPort = 80
          protocol      = "tcp"
          hostPort      = 80
        }
      ]
    }
  ])
}

# ECS-06
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group
resource "aws_security_group" "demo-task-security-group-01" {
  name   = "demo-task-security-group-01"
  vpc_id = aws_vpc.demo-vpc-01.id

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    # cidr_blocks = ["0.0.0.0/0"]
    security_groups = [aws_security_group.demo-lb-security-group-01.id]
  }
  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# ECS-05
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_service
resource "aws_ecs_service" "demo-hello-world-service" {
  name            = "demo-hello-world-service"
  cluster         = aws_ecs_cluster.demo-fargate-cluster.id
  task_definition = aws_ecs_task_definition.demo-nginx-task-definition.arn
  desired_count   = 2
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = [aws_security_group.demo-task-security-group-01.id]
    subnets         = [aws_subnet.demo-public-sub-01.id,aws_subnet.demo-public-sub-02.id]
    assign_public_ip = true
    #assign_public_ip = false
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.demo-target-group-01.id
    container_name   = "demo-test-container"
    container_port   = 80
  }

  depends_on = [aws_lb_listener.demo-lb-lis-01]

}

resource "aws_ecs_cluster" "demo-fargate-cluster" {
  name = "demo-fargate-cluster"
}


# ECS-03
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ecs_cluster

resource "aws_security_group" "demo-lb-security-group-01" {
  name   = "demo-lb-security-group-01"
  vpc_id = aws_vpc.demo-vpc-01.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# ECS-02
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group
resource "aws_lb" "demo-lb-01" {
  name            = "demo-lb-01"
  subnets         = [aws_subnet.demo-public-sub-01.id, aws_subnet.demo-public-sub-02.id]
  security_groups = [aws_security_group.demo-lb-security-group-01.id]
}


# ALB
# ECS-01
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb
resource "aws_lb_target_group" "demo-target-group-01" {
  name        = "demo-target-group-01"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.demo-vpc-01.id
  target_type = "ip"
}

# ECS-02
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener
resource "aws_lb_listener" "demo-lb-lis-01" {
  load_balancer_arn = aws_lb.demo-lb-01.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.demo-target-group-01.id
    type             = "forward"
  }
}


output "load_balancer_ip" {
  value = aws_lb.demo-lb-01.dns_name
}