<br>  
<br>    
📌

# **<image src='./images/terraform.svg' width="5%" hight="5%"/>Terraform Create AWS ECS**  

## **🎯 Demo Goal**
- **<h2>Network</h2>**
  
  <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content"  width="100%"><image src='./images/vpc1.jpg'   width="800px"> </div>

<br>  
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌



- **<h2>ALB+ECS</h2>**
  
  <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content"  width="100%"><image src='./images/ecs1.jpg'   width="800px"> </div>

<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌

## **🎯 Use Terraform to Create VPC and ECS**


- **<h2>✅Prerequisites</h2>**
  - 🎥[Install Terraform on Windows 11](https://youtu.be/5D76gEuAujk)
  - 🎥[Install Terraform on Ubuntu 23](https://youtu.be/f5BXa6Xk1Ws)
  - 🎥[Configure AWS CLI](https://youtu.be/P10P2NrpzpU)
- **<h2>🔎Clear Requirement</h2>**
  
- **<h2>✍🏼Create Target VPC via AWS UI</h2>**
  
  - 🎥[AWS VPC  Subnets For Beginners 2023](https://youtu.be/WWp3TXFPvSc) 


- **<h2>✍Create Target ALB + ECS via AWS UI</h2>**
  
  - 🎥[How to Create an Application Load Balancer with AWS ECS Fargate?](https://youtu.be/gVTdcR5bdUk)
  

  
- **<h2>🌘Use Terraform to Create VPC + ALB + ECS</h2>**
  - main.ft 
  - `terraform apply` 
  - `terraform destroy`

<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌

