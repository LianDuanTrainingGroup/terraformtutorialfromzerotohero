📌

# **<image src='./images/terraform.svg' width="5%" hight="5%"/>Terraform AWS Resource**  
## **🎯Topics**    


- **<h2>🤔What is Terraform AWS Resource?</h2>** 
- **<h2>❓How to Add Terraform AWS Resource?</h2>**
 
- **<h2>🔃What is the Lifecycle of Terraform AWS Resource?</h2>**
  
- **<h2>🎡AWS Resource Dependencies</h2>**

<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>

<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌



## **🤔What is Terraform AWS Resource?**  

- **<h2>👨‍💻Provision and management within the Amazon Web Services (AWS) infrastructure. </h2>**
 
- **<h2>🛠️Set of properties, configuration options, and associated APIs for management.</h2>**
- **<h2>📚[AWS Provider](https://registry.terraform.io/providers/hashicorp/aws/latest/docs)</h2>**

<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>

<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌



## **❓How to Add Terraform AWS Resource?**  

- **<h2>➕Add AWS Provider</h2>**

   - 🎞️[Terraform Provider Upgrade](https://youtu.be/aifjUTBiPyg)


- **<h2>➕Add AWS Resource</h2>**


```
resource "<provider>_<resource_type>" "<resource_name>" {
      <property1> = <value1>
      <property2> = <value2>
      // Additional properties...
}
```
  <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content"  width="100%"><image src='./images/example.jpg'   width="700px" style="vertical-align:top"> </div>
</br>


 - **<h2>🎞️[Using Terraform Create AWS ECS](https://youtu.be/-pMSPNaAEPA)  </h2>**


<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌




## **🔃What is the Lifecycle of Terraform AWS Resource?**


- **<h2>✍🏼Create</h2>**

    ```
    terraform apply
    ```

    <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content;height: fit-content"  width="100%"><image src='./images/create.jpg'   width="700px" style="vertical-align:top"> </div>

- **<h2>🤹‍♂️Manage</h2>**
    ```
    terraform state list
    terraform state show <resource_type>.<resource_name>
    ```

    <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content;height: fit-content"  width="100%"><image src='./images/show.jpg'   width="700px" style="vertical-align:top"> </div>

  - terraform.tfstate and terraform.tfstate.backup
  
    <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content;height: fit-content"  width="100%"><image src='./images/status.jpg'   width="700px" style="vertical-align:top"> </div>

- **<h2>🛠️Update</h2>**

    ```
    terraform plan
    terraform apply
    ```

    <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content;height: fit-content"  width="100%"><image src='./images/update.jpg'   width="700px" style="vertical-align:top"> </div>

   - Destroy and Create
   
    <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content;height: fit-content"  width="100%"><image src='./images/des-create.jpg'   width="700px" style="vertical-align:top"> </div>
    
- **<h2>💥Destroy</h2>**

   ```
    terraform plan
    terraform destroy
  ```



    <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content;height: fit-content"  width="100%"><image src='./images/des.jpg'   width="700px" style="vertical-align:top"> </div>


<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌

## **🎡AWS Resource Dependencies**

- **<h2>🦸Implicit Dependencies:</h2>**
  - **When the relationship between resources is inherent**
  - **Simpler configurations**
  - **Resource types that inherently depend on others**

- **<h2>🦹Explicit Dependencies: </h2>**
  - **Non-standard or complex dependencies**
  - **Cross-resource dependencies**
  - **Order-sensitive operations**
  - **Avoiding resource race conditions**