<br>
<br>
📌

# **<image src='./images/terraform.svg' width="5%" hight="5%"/>How to Use Terraform Variables File?**  


## **🎯Topics**
- **<h2>🪂What is Terraform Variables File?</h2>**
  
- **<h2>✍️Hands-on Demo Prerequisites</h2>**
 
- **<h2>🎖️Create and Use Terraform Variables File</h2>**
  
- **<h2>🔀Move Variables Definition from main.tf to variables.tf</h2>**
  
- **<h2>🗃️How to Organize Terraform Variables Files for Multiple ENV?</h2>**

<br>  
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌


## **🪂What is Terraform Variables File?**
- **<h2>⚡Variables file with a .tfvars extension</h2>**


- **<h2>🗝️Contains key-value pairs </h2>**


- **<h2>🖖variables.tf vs .tfvars </h2>**


<br>  
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌




## **✍️Hands-on Demo Prerequisites**
-  **<h2>🎥[Install Terraform on Windows 11](https://youtu.be/5D76gEuAujk)</h2>**
-  **<h2>📽️[Install Terraform on Ubuntu 23](https://youtu.be/f5BXa6Xk1Ws)</h2>**
-  **<h2>📽 [Configure AWS CLI](https://youtu.be/P10P2NrpzpU)</h2>**
  
-  **<h2>🎦[How to Use Terraform Variable?](https://youtu.be/P10P2NrpzpU)  -> main.tf</h2>**


  


<br>  
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌


## **🎖️Create and Use Terraform Variables File**

- **<h2>📝dev-networking.tfvars </h2>**
  
  <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content"  width="100%"><image src='./images/content.jpg'   width="300px" style="vertical-align:top"> </div>
</br>  

- **<h2>🤖terraform apply -var-file="dev-networking.tfvars" </h2>**
 
- **<h2>🤖terraform destroy -var-file="dev-networking.tfvars"   </h2>**

<br>  
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌




## **🔀Move Variables Definition from main.tf to variables.tf**


- **<h2>📝variables.tf</h2>**


  <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content"  width="100%"><image src='./images/vfile.jpg'   width="800px" style="vertical-align:top"> </div>
</br>

- **<h2>🤖terraform apply -var-file="dev-networking.tfvars" </h2>**
 
- **<h2>🤖terraform destroy -var-file="dev-networking.tfvars"   </h2>**


<br>  
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌


## **🗃️How to Organize Terraform Variables Files for Multiple ENV?**

</br>

  <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content"  width="100%"><image src='./images/mfile.jpg'   width="300px" style="vertical-align:top"> </div>
</br>


- **<h2>🤖terraform apply -var-file="dev/networking.tfvars" </h2>**
  
- **<h2>🤖terraform destroy -var-file="dev/networking.tfvars"  </h2>**
  
- **<h2>📚[Terraform Workspaces](https://developer.hashicorp.com/terraform/cli/workspaces#alternatives-to-workspaces)</h2>**




<br>  
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌

