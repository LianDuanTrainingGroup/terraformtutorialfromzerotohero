<br>
<br>
📌

# **<image src='./images/terraform.svg' width="5%" hight="5%"/>How to Use Terraform Variable?**  


## **🎯Topics**
- **<h2>🌟Why do We Need Terraform Variable?</h2>**
     
- **<h2>🔡Variable Declaration </h2>**
 
- **<h2>✍️Hands-on Demo</h2>**
<br>  
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌 

## **🌟Why We Need Terraform Variable?**   

- **<h2>Parameterize infrastructure code</h2>**
 
- **<h2>Flexible and reusable across different environments or deployments</h2>**
  
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌


## **🔡Variable Declaration** 
```
variable "variable_name" {
  type        = string
  description = "Description of the variable."
  default     = "default_value"
  validation {
    condition     = <condition>
    error_message = "Error message when the condition is not met."
  }
}
```

- **Data Types**
   - **String**
  ```
  variable "example_string" {
    type = string
    default = "Hello, World!"
  }
  ```

   - **Number**
   ```
   variable "example_number" {
    type = number
    default = 66
  }
   ```

   - **Bool**
  ```
  variable "example_bool" {
    type = bool
    default = true
  }
  ``` 


   - **List (Tuple)**
  ```
  variable "example_list" {
      type = list(string)
      default = ["apple", "banana", "orange"]
   }

  ``` 

   - **Map (Object)**
  ```
  variable "example_map" {
      type = map(string)
      default = {
        key1 = "value1"
        key2 = "value2"
        key3 = "value3"
      }
   }
  ```


<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌


## **✍️Hands-on Demo** 

- **<h2>✅Prerequisites</h2>**
  - 🎥[Install Terraform on Windows 11](https://youtu.be/5D76gEuAujk)
  - 🎥[Install Terraform on Ubuntu 23](https://youtu.be/f5BXa6Xk1Ws)
  - 🎥[Configure AWS CLI](https://youtu.be/P10P2NrpzpU)

- **<h2>🎞️[Using Terraform Create AWS ECS](https://youtu.be/-pMSPNaAEPA) -> main.tf</h2>**
 
- **<h2>🧮Add Two Variables into main.tf</h2>**
 
```
variable "vpc_name" {
  type        = string
  description = "Name of the AWS VPC"
  validation {
    condition     = can(regex("^([a-zA-Z0-9-_]+)$", var.vpc_name))
    error_message = "Invalid AWS resource name. Only alphanumeric characters, hyphens, and underscores are allowed."
  }
}


variable "vpc_cidr_block" {
  type        = string
  description = "CIDR block for the VPC"
  validation {
    condition     = can(regex("^([0-9]{1,3}\\.){3}[0-9]{1,3}/[0-9]{1,2}$", var.vpc_cidr_block))
    error_message = "Invalid VPC CIDR block."
  }
}
```