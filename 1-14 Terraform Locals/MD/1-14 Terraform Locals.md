
<br>
<br>
📌

# **<image src='./images/terraform.svg' width="5%" hight="5%"/>Terraform Locals**  


## **🎯Topics**
- **<h2>🪂What are Terraform Locals?</h2>**
  
- **<h2>🎖️Locals VS Variables</h2>**
 
- **<h2>✍️Hands-on Demo</h2>**
  
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌



## **🪂What are Terraform Locals?** 
- **<h2>🏪Used to store intermediate values or perform calculations based on other variables</h2>**
  
- **<h2>📌Local variables are not exposed to the outside</h2>**
  
- **<h2>🔓Only accessible in the configuration file where they are defined </h2>**
  
- **<h2>📊Data Types</h2>**
   - String, Number, Boolean, List, Map, and Object. 
  
- **<h2>🔡Syntax</h2>**  

  <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content"  width="100%"><image src='./images/code1.jpg'   width="400px" style="vertical-align:top"> </div>




- **<h2>📝Example</h2>** 
 
  - **${local.variable_name}**  

  <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content"  width="100%"><image src='./images/code2.jpg'   width="600px" style="vertical-align:top"> </div>

<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌




##  **🎖️Locals VS Variables** 
- **<h2>🔎Scope</h2>**
  
- **<h2>👁️‍🗨️Visibility</h2>**
  
- **<h2>💫Purpose</h2>**
  
- **<h2>🎡Override</h2>**
  

<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌

 
##  **✍️Hands-on Demo** 
- **<h2>📺Prerequisites</h2>** 
  - **<h2>🎥[Install Terraform on Windows 11](https://youtu.be/5D76gEuAujk)</h2>**
  - **<h2>📽️[Install Terraform on Ubuntu 23](https://youtu.be/f5BXa6Xk1Ws)</h2>**
  - **<h2>📽 [Configure AWS CLI](https://youtu.be/P10P2NrpzpU)</h2>**
  - **<h2>🎦[How to Use Terraform Variable?](https://youtu.be/P10P2NrpzpU)</h2>**
  - **<h2>🎥[How to Use Terraform Variables File?](https://youtu.be/P10P2NrpzpU)  -> main.tf</h2>**
- **<h2>⚙️Use local value in AWS resources tag name or AWS resource name</h2>** 
 
- **<h2>🤖terraform validate</h2>** 
 
- **<h2>🤖terraform plan -var="env-name=dev" -var="app-version=v01" -var-file="dev/networking.tfvars" </h2>** 
  
- **<h2>🤖terraform apply -var="env-name=dev" -var="app-version=v01" -var-file="dev/networking.tfvars" </h2>** 
  


<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌
