# //////////////////////////////
#          Variables
# //////////////////////////////

variable "env-name" {
  type        = string
  description = "ENV Name" // such as: qa, dev, and prod
  validation {
    condition     = can(regex("^([a-zA-Z0-9-_]+)$", var.env-name))
    error_message = "Invalid ENV name. Only alphanumeric characters, hyphens, and underscores are allowed."
  }
}

variable "app-version" {
  type        = string
  description = "APP Version" // such as: qa, dev, and prod
  validation {
    condition     = can(regex("^([a-zA-Z0-9-_]+)$", var.app-version))
    error_message = "Invalid APP Version. Only alphanumeric characters, hyphens, and underscores are allowed."
  }
}


variable "vpc_name" {
  type        = string
  description = "Name of the AWS VPC"
  validation {
    condition     = can(regex("^([a-zA-Z0-9-_]+)$", var.vpc_name))
    error_message = "Invalid AWS resource name. Only alphanumeric characters, hyphens, and underscores are allowed."
  }
}


variable "vpc_cidr_block" {
  type        = string
  description = "CIDR block for the VPC"
  validation {
    condition     = can(regex("^([0-9]{1,3}\\.){3}[0-9]{1,3}/[0-9]{1,2}$", var.vpc_cidr_block))
    error_message = "Invalid VPC CIDR block."
  }
}
