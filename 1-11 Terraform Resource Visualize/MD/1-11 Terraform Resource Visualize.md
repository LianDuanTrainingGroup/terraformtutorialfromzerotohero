📌

# **<image src='./images/terraform.svg' width="5%" hight="5%"/>Terraform Resource Visualize**  



# **📊Why Visualize Terraform Resources?**

- **<h2>🔍Understanding and Clarity</h2>**
  
- **<h2>🪲Troubleshooting and Debugging</h2>**
 
- **<h2>📚Documentation and Communication</h2>**
  
- **<h2>🧑‍💻Planning and Optimization</h2>**
  
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>

<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌

# **🌌How to Visualize Terraform Resources?**   


- **<h2>📢Terraform's Built-in Graph Command</h2>**     
    - **<h2>🎞️[Using Terraform Create AWS ECS](https://youtu.be/-pMSPNaAEPA) -> main.tf</h2>**
    - **<h2>`terraform graph  > graph.json`</h2>**



- **<h2>🔮Visualization</h2>**


  - **<h2>[GraphvizOnline](https://github.com/dreampuf/GraphvizOnline)</h2>**
 
  - **<h2>[terraform-graph-beautifier](https://github.com/pcasteran/terraform-graph-beautifier)</h2>**

```
      terraform graph | docker run --rm -i --name terraform-graph-beautifier  ghcr.io/pcasteran/terraform-graph-beautifier:latest-linux --output-type=cyto-html > config.html
```
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>

<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌