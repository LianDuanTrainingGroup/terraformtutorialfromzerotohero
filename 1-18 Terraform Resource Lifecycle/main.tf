# //////////////////////////////
#          PROVIDERS
# //////////////////////////////
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.72.1"
    }
  }     
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "main_vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "DemoVPC"
  }
}

# Public Subnet 1 in us-east-1a
resource "aws_subnet" "public_subnet_1" {
  vpc_id                  = aws_vpc.main_vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"

  tags = {
    Name = "PublicSubnet_1"
  }
}

# Public Subnet 2 in us-east-1b
resource "aws_subnet" "public_subnet_2" {
  vpc_id                  = aws_vpc.main_vpc.id
  cidr_block              = "10.0.3.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1b"

  tags = {
    Name = "PublicSubnet_2"
  }
}

# Private Subnet 1 in us-east-1a
resource "aws_subnet" "private_subnet_1" {
  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "PrivateSubnet_1"
  }
}

# Private Subnet 2 in us-east-1b
resource "aws_subnet" "private_subnet_2" {
  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = "10.0.4.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "PrivateSubnet_2"
  }
}

resource "aws_internet_gateway" "internet_gw" {
  vpc_id = aws_vpc.main_vpc.id

  tags = {
    Name = "InternetGateway"
  }
}

resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.main_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gw.id
  }

  tags = {
    Name = "PublicRouteTable"
  }
}

# Associate the public route table with both public subnets
resource "aws_route_table_association" "public_assoc_1" {
  subnet_id      = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.public_rt.id
}

resource "aws_route_table_association" "public_assoc_2" {
  subnet_id      = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.public_rt.id
}

resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.public_subnet_1.id

  tags = {
    Name = "NATGateway"
  }
}

resource "aws_eip" "nat_eip" {
  depends_on = [aws_internet_gateway.internet_gw]  # Ensure the IGW is created before the EIP

  tags = {
    Name = "NATEIP"
  }
}

resource "aws_route_table" "private_rt" {
  vpc_id = aws_vpc.main_vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw.id
  }

  tags = {
    Name = "PrivateRouteTable"
  }
}

# Associate the private route table with both private subnets
resource "aws_route_table_association" "private_assoc_1" {
  subnet_id      = aws_subnet.private_subnet_1.id
  route_table_id = aws_route_table.private_rt.id
}

resource "aws_route_table_association" "private_assoc_2" {
  subnet_id      = aws_subnet.private_subnet_2.id
  route_table_id = aws_route_table.private_rt.id
}

# Rest of your resources remain the same...


resource "aws_security_group" "public_sg" {
  name = "public_sg"
  vpc_id = aws_vpc.main_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

#  ingress {
#    from_port   = 80
#    to_port     = 80
#    protocol    = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
#  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "PublicSecurityGroup"
  }
  description="For EC2 public Instance"
}



resource "tls_private_key" "my_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "my_key" {
  key_name   = "my-key"
  public_key = tls_private_key.my_key.public_key_openssh

  tags = {
    Name = "MyKeyPair"
  }

}

resource "local_file" "private_key" {
  content         = tls_private_key.my_key.private_key_pem
  filename        = "${path.module}/my-key.pem"
  file_permission = "0400"
}

data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "ec2_a" {
  #ami               = data.aws_ami.amazon_linux.id
  ami               = "ami-0866a3c8686eaeeba"
  instance_type     = "t2.micro"
  subnet_id         = aws_subnet.public_subnet_1.id
  security_groups   = [aws_security_group.public_sg.id]
  #vpc_security_group_ids  = [aws_security_group.public_sg.id]
  lifecycle {
    ignore_changes = [security_groups] # Ignore changes to security groups
  }
  
  
  key_name          = aws_key_pair.my_key.key_name  

  user_data = <<-EOF
#!/bin/bash
sudo apt-get update -y
sudo apt-get install -y unzip
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
curl -LO "https://dl.k8s.io/release/v1.31.0/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
  EOF

  tags = {
    Name = "EC2_SSH_Server"
  }
}

resource "aws_security_group" "private_sg" {
  vpc_id = aws_vpc.main_vpc.id
  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.public_sg.id]
    cidr_blocks = [aws_vpc.main_vpc.cidr_block] # Allow traffic from the whole VPC
  }
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.public_sg.id]
    cidr_blocks = [aws_vpc.main_vpc.cidr_block] # Allow traffic from the whole VPC
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "PrivateSecurityGroup"
  }
}

resource "aws_instance" "ec2_b" {
  ami               = data.aws_ami.amazon_linux.id
  instance_type     = "t2.micro"
  subnet_id         = aws_subnet.private_subnet_1.id
  security_groups   = [aws_security_group.private_sg.id]
  key_name          = aws_key_pair.my_key.key_name  

  user_data = <<-EOF
    !/bin/bash
    sudo yum install -y httpd
    sudo systemctl start httpd
    sudo systemctl enable httpd
    echo "<html><body><h1 style='color:red;'>Hello World from EC2 Instance B</h1></body></html>" > /var/www/html/index.html
  EOF
  
  depends_on = [aws_route_table_association.private_assoc_1, aws_security_group.private_sg]

  tags = {
    Name = "EC2_WEB_Server"
  }
}

# Output the public IP of EC2 Instance A for SSH access
output "ec2_a_public_ip" {
  value = aws_instance.ec2_a.public_ip
}

# Output the private IP of EC2 Instance B
output "ec2_b_private_ip" {
  value = aws_instance.ec2_b.private_ip
}

/*
import {
  to = aws_instance.example
  id = "i-0bfc3cad400f7339f"
}

resource "aws_instance" "example" {
  ami               = "ami-0866a3c8686eaeeba"
  instance_type     = "t2.micro"
  # (other resource arguments...)
}
*/


