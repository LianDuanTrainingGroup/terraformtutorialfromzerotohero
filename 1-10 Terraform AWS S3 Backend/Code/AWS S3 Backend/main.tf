# //////////////////////////////
#          Providers
# //////////////////////////////
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.8.0"
    }
  }
}
# Configure the AWS Provider
provider "aws" {
  region = "us-east-2"

  default_tags {
    tags = {
      Name = "create-by-terraform-need-tag"
    }
  }
}

# //////////////////////////////
#          Resources
# //////////////////////////////

# https://registry.terraform.io/providers/hashicorp/aws/5.8.0/docs/resources/s3_bucket

resource "aws_s3_bucket" "demo-bucket-01" {
  bucket = "demo-bucket-lian"
  force_destroy = true 
  lifecycle {
    prevent_destroy = false
  }
  tags = {
    Name        = "demo-bucket-lian"
    Environment = "dev"
  }
}


resource "aws_s3_bucket_versioning" "s3-versioning-01" {
  bucket = aws_s3_bucket.demo-bucket-01.id
  versioning_configuration {
    status = "Enabled"
  }
}

# https://registry.terraform.io/providers/hashicorp/aws/5.8.0/docs/resources/s3_bucket_ownership_controls

resource "aws_s3_bucket_ownership_controls" "demo-s3-bucket-ownership-01" {
  bucket = aws_s3_bucket.demo-bucket-01.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

# https://registry.terraform.io/providers/hashicorp/aws/5.8.0/docs/resources/s3_bucket

resource "aws_s3_bucket_acl" "s3-bucket-acl-01" {
  depends_on = [aws_s3_bucket_ownership_controls.demo-s3-bucket-ownership-01]
  bucket = aws_s3_bucket.demo-bucket-01.id
  acl    = "private"
}

# https://registry.terraform.io/providers/hashicorp/aws/5.8.0/docs/resources/dynamodb_table

resource "aws_dynamodb_table" "backend_locks" {
  name           = "demo-dynamodb-table-01"
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name        = "demo-dynamodb-table-01"
    Environment = "dev"
  }
}



