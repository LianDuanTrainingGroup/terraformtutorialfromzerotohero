# //////////////////////////////
#          Providers
# //////////////////////////////
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.8.0"
    }
  }
backend "s3" {
    bucket = "demo-bucket-lian"
    region = "us-east-2"
    dynamodb_table = "demo-dynamodb-table-01"
  }
   
}
# Configure the AWS Provider
provider "aws" {
  region = "us-east-2"

  default_tags {
    tags = {
      Name = "create-by-terraform-need-tag"
    }
  }
}


# //////////////////////////////
#          Resources
# //////////////////////////////

# VPC-01
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc

resource "aws_vpc" "demo-vpc-01" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "demo-vpc-01"
  }
}
# VPC-02
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet
resource "aws_subnet" "demo-public-sub-01" {
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-east-2a"
  vpc_id                  = aws_vpc.demo-vpc-01.id
  map_public_ip_on_launch = true
  tags = {
    Name = "demo-public-sub-01"
  }
}



# VPC-03
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet
resource "aws_subnet" "demo-public-sub-02" {
  cidr_block              = "10.0.8.0/24"
  availability_zone       = "us-east-2b"
  vpc_id                  = aws_vpc.demo-vpc-01.id
  map_public_ip_on_launch = true
  tags = {
    Name = "demo-public-sub-02"
  }
}

output "vpc_detail" {
  value = aws_vpc.demo-vpc-01
}

