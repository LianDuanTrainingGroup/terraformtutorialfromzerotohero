📌

# **<image src='./images/terraform.svg' width="5%" hight="5%"/>How to Use Terraform Cloud Backend with AWS S3?**  

## **🗄️What is Terraform AWS S3 Backend?**
- **<h2>Local Backend 🆚 Cloud Backend</h2>**


- **<h2>📔Terraform state files are in AWS S3 Backend</h2>**


  <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content"  width="100%"><image src='./images/files.png'   width="500px" style="vertical-align:top"> </div>

<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌




## **👉Hands-on Demos**


**<h2>🎯Prerequisites:</h2>**   



-  **<h2>🎥[Install Terraform on Windows 11](https://youtu.be/5D76gEuAujk)</h2>**
   
-  **<h2>📽️[Install Terraform on Ubuntu 23](https://youtu.be/f5BXa6Xk1Ws)</h2>**
  
-  **<h2>📽 [Configure AWS CLI](https://youtu.be/P10P2NrpzpU)</h2>**
-  **<h2>🏷️Terraform AWS Providers Version: 5.8.0</h2>**

<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌




**<h2>✍🏼Create a Terraform AWS S3 Backend</h2>**


**<h2>📋S3 Backend Resource List:</h2>**


   - **<h3>📊aws_dynamodb_table.backend_locks</h3>**
   
   - **<h3>🪣aws_s3_bucket.demo-bucket-01</h3>**
     - **aws_s3_bucket_acl.s3-bucket-acl-01**
     - **aws_s3_bucket_ownership_controls.demo-s3-bucket-ownership-01**
     - **aws_s3_bucket_versioning.s3-versioning-01**


<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌





**<h2>✍🏼Create a VPC with AWS S3 Backend</h2>**  

```
terraform {
 ...
   backend "s3" {
    bucket = "demo-bucket-lian"
    region = "us-east-2"
    dynamodb_table = "demo-dynamodb-table-01"
  }
}
```

<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌

