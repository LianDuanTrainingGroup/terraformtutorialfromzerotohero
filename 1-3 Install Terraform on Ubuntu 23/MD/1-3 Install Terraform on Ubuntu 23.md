# **<image src='./images/terraform.svg' width="5%" hight="5%"/>Install Terraform on <image src='./images/logo_ubuntu_icon.png' width="3%" hight="5%"/>Ubuntu 23**



# **📢Install Terraform Commands**

```
🍫 Install Dependencies Packages
sudo apt-get update && sudo apt-get install -y gnupg 

🗝️ Install HashiCorp GPG Key
wget -O- https://apt.releases.hashicorp.com/gpg | \
gpg --dearmor | \
sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg

💾 Add Official HashiCorp Repository
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt-get update

🧑🏻‍💻 Install Terraform
sudo apt-get install terraform
```

<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌

# **✅Precondition Check**
`lsb_release -a`

- <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/ubuntu2.jpg'  width="300px" style="vertical-align:top"></div>


<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


# **🍫Install Dependencies Packages** 
`sudo apt-get update && sudo apt-get install -y gnupg`
 - gnupg allows encrypt and sign data and communications.

- <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/packages.jpg'  width="900px" style="vertical-align:top"></div>


  - Does software-properties-common Need?  
    - No
<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌




# **🗝️Install HashiCorp GPG Key**
```
wget -O- https://apt.releases.hashicorp.com/gpg | \
gpg --dearmor | \
sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
```
- wget -O- https://apt.releases.hashicorp.com/gpg
  - O option tells wget to output the downloaded content to stdout.
- gpg --dearmor
  -  Converts the ASCII-armored GPG public key to its binary format.
- sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
  - Writes it to the file /usr/share/keyrings/hashicorp-archive-keyring.gpg. 

<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌



# **💾Add Official HashiCorp Repository**

```
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
sudo tee /etc/apt/sources.list.d/hashicorp.list
```
- <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/repository.jpg'  width="900px" style="vertical-align:top"></div>


- echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main": 
  - Tells APT to verify the packages using the HashiCorp public key that was installed in the previous step.
- sudo tee /etc/apt/sources.list.d/hashicorp.list
  - tee command writes the output from the previous command to the file /etc/apt/sources.list.d/hashicorp.list. 
  - hashicorp.list is used by the APT package manager to locate and download packages from the HashiCorp repository. 

- Verify terraform package   
  `sudo apt-get update && sudo apt info terraform`

- If you face error "Unable to location package terraform", run commands:
  ```
  echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
  https://apt.releases.hashicorp.com jammy main" | \
  sudo tee /etc/apt/sources.list.d/hashicorp.list
  sudo apt-get update
  ```
     - <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/error.jpg'  width="500px" style="vertical-align:top"></div>


<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌


# **🧑🏻‍💻Install Terraform**

`sudo apt-get install terraform`  

- <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/install.jpg'  width="600px" style="vertical-align:top"></div>


-  Verify Terraform

    - `terraform -version`

    - <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/version.jpg'  width="450px" style="vertical-align:top"></div>


<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌

