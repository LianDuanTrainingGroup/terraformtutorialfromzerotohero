<br>      
📌



# **<image src='./images/terraform.svg' width="5%" hight="5%"/>Terraform Provider Upgrade**
# **📆Topics**


- **<h2>🔨What is Terraform Provider?</h2>**
  
- **<h2>🏷️What are Terraform Provider Version Constraints?</h2>**
  
- **<h2>⬆️How to Upgrade Terraform Provider Version?</h2>**




<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌


# **🔨What is Terraform Provider?**


- **<h2>🧑‍💻Manage resources on a particular infrastructure platform.</h2>** 
  - Amazon Web Services (AWS)
  - Microsoft Azure
  - Google Cloud Platform (GCP)

- **<h2>🤖Translate Terraform configurations into API calls to create, update, or delete resources.</h2>** 

<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌




# **🏷️What are Terraform Provider Version Constraints?**

- **<h2>🚧Version Constraints</h2>**
    - `=`  
    - `!=`  
    - `>, >=, <, <=` 
    - `~>`   
      - `~> 4.65.0` (>= 4.65.0 and < 4.66.0)


-  **<h2>🧱Version Constraints in Providers Block</h2>**
   - main.tf ([Use Terraform to Create an AWS VPC]())

```
terraform {
      required_providers {
         aws = {
            source  = "hashicorp/aws"
            version = "4.66.0"
         }
      }     
   }
```
- ⌨️`terraform init` 
- 💾.terraform.lock.hcl has version and constraints


<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌



# **⬆️How to Upgrade Terraform Provider Version?**


- **<h2>Upgrade Strategy</h2>**  
  
  - Bug trigger 
  - Frequently
- **<h2>Get Release Info</h2>**
  - Review Terraform Provider version changes
     - Check https://github.com/hashicorp/terraform-provider-aws/releases for AWS provider changes
  - Add release notifications for the provider you are using
- **<h2>Run `terraform init -upgrade` to upgrade to the target provider version</h2>**
```
terraform {
      required_providers {
         aws = {
            source  = "hashicorp/aws"
            version = "4.66.1"
         }
      }     
   }
```

<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌