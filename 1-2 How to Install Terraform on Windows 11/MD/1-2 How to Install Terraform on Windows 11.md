# **<image src='./images/terraform.svg' width="5%" hight="5%"/>How to Install Terraform on Windows 11 ? **
# **📦Topics** 

  -  **<h2>✅Precondition Check</h2>**


  -  **<h2>🕵️‍♂️Install Terraform - Chocolatey VS Manual</h2>**


  -  **<h2>🍫Install & Upgrade Chocolatey</h2>**


  -  **<h2>🚀Install Terraform via Chocolatey</h2>**
  
  -  **<h2>🌌How to Install Specific Terraform Version?</h2>**

<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌


# **✅Precondition Check**
- **<h2>🪟My demo box is on Microsoft Windows 11 Pro (10.0.22621).</h2>**


    - **<h2>`systeminfo` </h2>**
    - <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/system.jpg'  width="600px" style="vertical-align:top"></div>

<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

# **🕵️‍♂️Install Terraform - Chocolatey VS Manual** 
  - **<h2>🤼‍♂️Chocolatey VS Manual Install Terraform </h2>**


    - 📝Chocolatey and the Terraform package are NOT directly maintained by HashiCorp. 
    - 🆕The latest version of Terraform is always available by manual installation.  

  - **<h2>✍️Manual Install Terraform </h2>**


    - 📥Download and Unzip Terraform EXE for Windows
      -  📚https://developer.hashicorp.com/terraform/downloads
  
    - 🔄Add Terraform EXE in Path Environment Variable
     
    - 📊Open a new Command Prompt Or Power Shell Testing to Run Terraform Command
  
       - `Terraform -version`


<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>    
<br>
<br>
<br>  
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


# **🍫Install/Upgrade Chocolatey**   

- **<h2>📥Install Chocolatey </h2>**


  - ⌨Open CMD as admin, and run the below command  
     - 📚https://docs.chocolatey.org/en-us/choco/setup     
``` 
"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "[System.Net.ServicePointManager]::SecurityProtocol = 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
```
  
- **<h2>🏹Upgrade Chocolatey </h2>**


   - `choco upgrade chocolatey`
   - <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/upgrate.jpg'  width="400px" style="vertical-align:top"></div>   

<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

# **🚀Install Terraform via Chocolatey**

```
choco install terraform 
terraform -version
```


- <div id="wrapper-div" style="box-shadow: 0px 2px 20px rgba(0, 0, 0, .25); width:fit-content; height: fit-content;"><image src='./images/install.jpg'  width="600px" style="vertical-align:top"></div>


<br>
<br>
<br>   
<br>
<br>
<br>      
📌

# **🌌How to Install Specific Terraform Version?**

```
choco uninstall terraform
choco search Terraform --all
choco install Terraform -version X.Y.Z
```